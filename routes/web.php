<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', 'HomeController@index');

Route::get('/register', 'AuthController@daftar');

Route::post('/welcome', 'AuthController@submit');

Route::get('/master', function(){
    return view('layout.master');
});

Route::get('/table', function(){
    return view('page.table');
});

Route::get('/datatable', function(){
    return view('page.datatable');
});


